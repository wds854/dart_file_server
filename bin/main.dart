/// This serves static files from a directory on an http port.

import 'dart:io';
import 'package:http_server/http_server.dart';

Future main(List<String> args) async {
  if (args.length != 2) {
    print('Expected 2 arguments: The path to the directory to serve and the port to serve on.');
    print('Example to serve the current directory on port 8080: serve . 8080');
    return;
  }
  
  String path = args[0];
  if (!Directory(path).existsSync()) {
    print('Directory "$path" does not exist.');
    return;
  }

  var port = int.tryParse(args[1]);
  if (port == null || port < 1023 || port > 65535) {
    print('Port must be an integer between 1023 and 65535.');
    return;
  }

  var staticFiles = VirtualDirectory(path);
  staticFiles.allowDirectoryListing = true;
  staticFiles.directoryHandler = (dir, request) {
    var indexUri = Uri.file(dir.path).resolve('index.html');
    staticFiles.serveFile(File(indexUri.toFilePath()), request);
  };

  var server = await HttpServer.bind(InternetAddress.loopbackIPv4, port);
  print('Listening on http://localhost:$port');
  await server.forEach(staticFiles.serveRequest);
}