# dart_file_server

This is a simple http server that serves up a given directory on a given port.

Example usage:
```
bin\main.dart \path\to\website\root 8080
```

Relative paths like ```website``` can also be passed.

## Building

Running ```scripts\build``` creates a build\serve.exe file

You can then call this executable like:
```
serve \path\to\website\root 8080
```
